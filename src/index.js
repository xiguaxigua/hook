const express = require('express');
const fs = require('fs');
const shell = require('shelljs');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

app.post('/ci', (req, res) => {
  const { headers, body } = req;
  if (headers['x-gitlab-token'] !== 'secretxigua') {
    res.status(401).send('secret wrong')
    return
  }
  cloneCode(req.body);
  res.send('POST request to homepage');
})

app.listen(3001, () => {
  console.log('Server is listening on 3001');    
})

function cloneCode (body) {
  if (!shell.which('git')) { shell.exit(1); }
  
  fs.exists(body.project.name, (exits) => {
    shell.cd('/root/workspace/hook/projects');
    if (exits) {
      console.log('git pull');
      shell.cd(body.project.name);
      shell.exec('git pull');
    } else {
      console.log('git clone');
      const gitClone = `git clone ${body.repository.url}`;
      console.log(gitClone);

      if (shell.exec(gitClone).code !== 0) {
        shell.echo('Error: Git clone failed');
        shell.exit(1);
      }
    }
    console.log('success');
  })
}
